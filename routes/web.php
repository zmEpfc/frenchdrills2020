<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/posts', 'PostController@index');
Route::get('/quiz-verb-multiple-choices', 'QuizController@quizVerbView')->name('quiz-create');
Route::post('/quizzes-create', 'QuizController@freeQuizCreate'); //free-quizz post user options form



//route créee durant le tuto grafikart
//Route::get('/blog/{post}', function(\App\Post $post) {
//    return $post;
//})->name('post.show');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');



//routes de quiz
Route::get('/quiz/{id}', 'QuizController@show')->name('quiz.show');
Route::post('/quiz-verb-save', 'QuizController@storeQuizVerb');
Route::post('/quiz-verb-teacher-multiple-update/{id}', 'QuizController@update');
Route::resource('quizzes', 'QuizController');

//routes des leçons
Route::get('/lesson/{id}', 'PostController@show')->name('post.show');
Route::post('/lesson-quiz-save', 'PostQuizController@storeQuizLesson');
Route::post('/lesson-is-already-answered', 'PostQuizController@isAlreadyAnswered');


//route parser
//Route::get('/xml-parser', 'XmlParserController@index');

//route goole audio
//Route::get('/google-text-speech', 'GoogleTextToSpeachController@infinitives');
//Route::get('/google-text-speech-present', 'GoogleTextToSpeachController@present');
//Route::get('/google-text-speech-imparfait', 'GoogleTextToSpeachController@imparfait');
//Route::get('/google-text-speech-passe', 'GoogleTextToSpeachController@passe');
//Route::get('/google-text-speech-conditionnel', 'GoogleTextToSpeachController@conditionnel');
//Route::get('/google-text-speech-futur', 'GoogleTextToSpeachController@futur');


//home student and home teacher
//Route::get('/home', 'HomeController@index')->name('home')->middleware('checkIsTeacher');
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/home-teacher', 'HomeController@indexTeacher')->name('home_teacher');
Route::post('/store-teacher-correction', 'HomeTeacherController@storeCorrection');

Route::get('/student-refresh-datas', 'HomeController@getRefreshedDatasStudent');

//route table de conjugaison
Route::post('/table-conjugation', 'ConjugationTableController@show');

Route::get('/statistics-worst-verbs', 'NovaController@getWorstVerbs');




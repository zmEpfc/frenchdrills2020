<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verbes', function (Blueprint $table) {
            $table->increments('id')->unique()->unsigned();
            $table->string('temps');
            $table->string('je');
            $table->string('tu');
            $table->string('il');
            $table->string('nous');
            $table->string('vous');
            $table->string('ils');

            $table->integer('infinitif_id')->unsigned();
            $table->foreign('infinitif_id')->references('id')->on('verbes_infinitif')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verbes');

    }
}

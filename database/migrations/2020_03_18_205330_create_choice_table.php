<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choices', function (Blueprint $table) {
            $table->increments('id')->unique()->unsigned();
            $table->tinyInteger('position')->unique()->unsigned();
            $table->string('label');
            $table->string('label_audio');
            $table->boolean('is_good_answer');
            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('choices', function (Blueprint $table) {
            Schema::dropIfExists('choices');
        });
    }
}

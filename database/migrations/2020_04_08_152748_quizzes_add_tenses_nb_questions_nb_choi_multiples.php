<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class QuizzesAddTensesNbQuestionsNbChoiMultiples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quizzes', function (Blueprint $table) {
            $table->enum('tenses', ['present', 'imparfait', 'futur', 'passe', 'conditionnel']);
            $table->tinyInteger('nb_questions')->default(0);
            $table->tinyInteger('nb_choices')->default(3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

/**
 *  Class Choice
 * This is the model class for table "policy"
 *
 * @property integer $id
 * @property integer $position
 * @property string $label
 * @property string $labelAudio
 * @property boolean $isGoodAnsswer
 */
class Choice extends Model
{
    static $exclude = [];
    static $conjugatedGoodAnswer;
    public static function buildChoices($currentPronoun, $pronounsAndValues, $nbChoices, $isMissingQuiz = false){

        $choices = [];
        //first we build the good answer
        $goodAnswer = self::buildGoodAnswer($currentPronoun, $pronounsAndValues);
        array_push($choices, $goodAnswer);
        if($isMissingQuiz){
            return $choices;
        }
        //remove goodAnswer from possible choices because we have already the good answer IN ARRAY $choices
        $pronounsAndValues = self::filterPronounsAndValue($currentPronoun, $pronounsAndValues);
        //remove any other value which can be the same as good answer for ex: "je place" "il place"
        $pronounsAndValues = self::filterValueDuplicate($pronounsAndValues);

        //randomize array of $pronounsAndValues
        $pronounsAndValuesRandomized = array_unique(self::randomizeAndKeepKeys($pronounsAndValues));

        $i=0;
        foreach($pronounsAndValuesRandomized as $pronounKey => $pronounValue)
        {

            if($i < $nbChoices - 1)
            {
                $choice = new Choice();
                $choice->position = $i + 1;
                $pronomWithApostropheOrNot = self::pronomForVoyelle($currentPronoun, $pronounsAndValuesRandomized[$pronounKey]);
                $space = $pronomWithApostropheOrNot === "J'" ? '' : ' ';
                $choice->label = self::pronomForVoyelle($currentPronoun, $pronounsAndValuesRandomized[$pronounKey]) . $space .$pronounsAndValuesRandomized[$pronounKey];
                $choice->isGoodAnswer = false;
                array_push($choices, $choice);
                $i++;
            } else{
                break;
            }
        }
        shuffle($choices);
        $choices = self::defineChoicesPosition($choices);

        return $choices;
    }

    // Renvoi Je ou J' selon le verbe (par ex pour être, écrire, etc)
    private static function pronomForVoyelle($pronom, $verbe){
        if($pronom != 'je'){
            return ucfirst($pronom);
        }


//        var_dump(utf8_decode($verbe));
//        var_dump(utf8_decode($verbe)[0]);
//        var_dump(utf8_decode('é'));
//        var_dump(utf8_decode($verbe)[0] === utf8_decode('é'));

        $mask = ['a', 'e', 'i', 'o', 'u', 'é', 'è', 'ê'];
        $mask = array_map(function($v){
            return utf8_decode($v);
        }, $mask);

        $firstCharOfVerb = utf8_decode($verbe)[0];
        if(in_array($firstCharOfVerb, $mask)){
            return "J'";
        } else {
            return 'Je';
        }
    }

    //define choice position after shuffle
    private static function defineChoicesPosition(&$choices)
    {
        for($i=0; $i<count($choices); ++$i){
            $choices[$i]->position = $i;
        }
        return $choices;
    }

    private static function buildGoodAnswer($currentPronoun, $pronounsAndValues)
    {
        $choice = new Choice();
        $pronomWithApostropheOrNot = self::pronomForVoyelle($currentPronoun, $pronounsAndValues[$currentPronoun]);
        $space = $pronomWithApostropheOrNot === "J'" ? '' : ' ';
        $choice->label = $pronomWithApostropheOrNot . $space . $pronounsAndValues[$currentPronoun];
        $choice->isGoodAnswer = true;
        $choice->position = 0;
        self::$conjugatedGoodAnswer = $pronounsAndValues[$currentPronoun];
        return $choice;
    }

    //remove good answer from the array
    private static function filterPronounsAndValue($currentPronoun, $pronounsAndValues)
    {
        return array_filter($pronounsAndValues, function($key) use ($currentPronoun) {
            return $key !== $currentPronoun;
        }, ARRAY_FILTER_USE_KEY);
    }

    //remove same value for exemple: je place, il place ...
    private static function filterValueDuplicate($pronounsAndValues)
    {
        return array_filter($pronounsAndValues, function($val) {

            return !(trim($val) === trim(self::$conjugatedGoodAnswer));
        });
    }

    private static function randomizeAndKeepKeys($pronounsAndValues) {
        if (!is_array($pronounsAndValues)) return $pronounsAndValues;

        $keys = array_keys($pronounsAndValues);
        shuffle($keys);
        $new = [];
        foreach ($keys as $key) {
            $new[$key] = $pronounsAndValues[$key];
        }
        return $new;
    }

    public function question(){
        return $this->belongsTo(Question::class);
    }

    private static function vv($data)
    {
        var_dump($data);
    }

}

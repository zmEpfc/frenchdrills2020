<?php
//
//namespace App\Nova;
//
//use Illuminate\Http\Request;
//use Laravel\Nova\Fields\ID;
//use Laravel\Nova\Http\Requests\NovaRequest;
//use Benjaminhirsch\NovaSlugField\Slug;
//use Benjaminhirsch\NovaSlugField\TextWithSlug;
//use Laravel\Nova\Fields\BelongsToMany;
//
//
//class Category extends Resource
//{
//
//    public static $model = 'App\Category';
//    public static $title = 'name';
//    public static $group = "Cours";
//
//    /**
//     * The columns that should be searched.
//     *
//     * @var array
//     */
//    public static $search = [
//        'name',
//    ];
//
//    /**
//     * Get the fields displayed by the resource.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function fields(Request $request)
//    {
//        return [
//            ID::make()->sortable(),
//            TextWithSlug::make('Titre', 'name')
//                ->slug('slug'),
//
//            Slug::make('slug', 'slug'),
//            BelongsToMany::make('Posts', 'posts', Post::class)
//        ];
//    }
//
//    /**
//     * Get the cards available for the request.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function cards(Request $request)
//    {
//        return [];
//    }
//
//    /**
//     * Get the filters available for the resource.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function filters(Request $request)
//    {
//        return [];
//    }
//
//    /**
//     * Get the lenses available for the resource.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function lenses(Request $request)
//    {
//        return [];
//    }
//
//    /**
//     * Get the actions available for the resource.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return array
//     */
//    public function actions(Request $request)
//    {
//        return [];
//    }
//}

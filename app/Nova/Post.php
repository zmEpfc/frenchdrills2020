<?php

namespace App\Nova;

use App\Nova\Actions\AttributeSetter;
use App\Nova\Filters\AuthorFilter;
use App\Question;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Http\Requests\NovaRequest;
use Manogi\Tiptap\Tiptap;
use NovaAttachMany\AttachMany;
use Whitecube\NovaFlexibleContent\Flexible;
use Spatie\TagsField\Tags;


class Post extends Resource
{
    public static $model = 'App\Post';
    public static $group = "Cours";

    public static $title = 'name';
     public static $search = [
        'name',
        'content'
    ];
//    public static $with = ['categories']; //eager loading

    public static function label()
    {
        return 'Cours';
    }

    public static function singularLabel()
    {
        return 'Cours';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
//            ID::make()->sortable(),
//            Text::make('URL', 'id', function ()
//            {
//                $route = route('post.show', $this->id);
//                return '<a href="' . $route . '">' . $route . '</a>';
//            })
//                ->asHtml()->exceptOnForms(),
//            Text::make('Titre', 'name')
//                ->onlyOnForms(),
            Image::make('Image', 'image')->disk('public'),


            Text::make('Titre', 'name', function ()
            {
                $route = route('post.show', $this->id);
                return '<a href="' . $route . '">' . $this->name . '</a>';
            })
            ->asHtml()->exceptOnForms(),


            TextWithSlug::make('Titre', 'name')
                ->slug('slug')->onlyOnForms(),
            Slug::make('slug', 'slug')->onlyOnForms(),
//            Text::make('Titre', function(){
//                if($this->id === null) return null;
//                $route = route('post.show', $this->id);
//                return <<<HTML
//                    <a href="$route" class="no-underline font-bold dim text-primary">$this->name</a>
//                HTML;
//            })->asHtml(),
//            Text::make('Catégories', function(){
//                return $this->categories->map(function(\App\Category $cat){
//                    return $cat->name;
//                })->implode(', ');
//            }),
//            Tags::make('Tags'),



            Tiptap::make('Contenu', 'content')  ->buttons([
                'heading',
                'italic',
                'bold',
                'code',
                'link',
                'strike',
                'underline',
                'bullet_list',
                'ordered_list',
                'code_block',
                'blockquote',
            ])
                ->headingLevels(3)->onlyOnForms(),

            Flexible::make('Liste de questions à Choix multiples', 'questions_choices_multiples')
                ->button('Ajouter une question à choix multiples')
                ->fullWidth()
                ->addLayout('', 'questions_choices_multiples', [
                    Markdown::make('Intitulé de la question', 'question_label'),
                    Text::make('Choix n°1' ,'choix1')
                    ->help('Entrez ici la bonne réponse'),
                    Text::make('Choix n°2', 'choix2'),
                    Text::make('Choix n°3', 'choix3'),
                    Text::make('Choix n°4', 'choix4'),
                    Text::make('Choix n°5', 'choix5'),
                ]),
            Flexible::make('Liste de questions ouvertes', 'questions_open')
                ->button('Ajouter une question ouverte')
                ->fullWidth()
                ->addLayout('', 'questions_open', [
                    Markdown::make('Intitulé de la question', 'question_label'),
                ]),

//            Boolean::make('En ligne', 'online'),
            DateTime::make('Date', 'created_at')->format('D-M-Y')->exceptOnForms(),

            AttachMany::make('Étudiants', 'users', User::class )
                ->fullWidth()
                ->help('<b>Etudiants ayant accès au quiz</b>')

//            AttachMany::make('Catégorie', 'Categories')
//                ->fullWidth()
//                ->help('<b>Etudiants ayant accès au quiz</b>'),
//            BelongsTo::make('Auteur', 'user', User::class)->exceptOnForms(),
//            BelongsToMany::make('Categories', 'categories', Category::class)
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new AuthorFilter()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {

        return [
//            new AttributeSetter('Mettre hors ligne', 'online', 0)
        ];
    }

//    public function relatableUsers (NovaRequest $request, $query) {
//        return $query->role('student');
//    }



}

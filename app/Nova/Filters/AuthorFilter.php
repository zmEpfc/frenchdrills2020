<?php

namespace App\Nova\Filters;

use App\User as User;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class AuthorFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    public $name = "Autheur";
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('author', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return User::whereHas('roles', function ($q) {
            $q->where('roles.name', '=', 'teacher');
        })->pluck('id', 'name')->toArray();
//        return User::pluck('id', 'name')->toArray();
    }
}

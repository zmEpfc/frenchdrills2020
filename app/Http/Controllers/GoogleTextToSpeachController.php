<?php


namespace App\Http\Controllers;

use App\Verbe;
use App\VerbeInfinitif;
use Illuminate\Http\Request;

class GoogleTextToSpeachController extends Controller
{
    public function index()
    {

    }

    public function infinitives(){
        $verbes = VerbeInfinitif::all()->pluck('label')->toArray();
        $verbes = str_replace(':', '', $verbes);

        dd($verbes);
    }

    public function present(){
        $verbesPresent = Verbe::where('temps', '=', 'present')
            ->get(['je', 'tu', 'il', 'nous', 'vous', 'ils'])
            ->toArray();
        dd($verbesPresent);
    }
    public function imparfait(){
        $verbesImparfait = Verbe::where('temps', '=', 'imparfait')
            ->get(['je', 'tu', 'il', 'nous', 'vous', 'ils'])
            ->toArray();
        dd($verbesImparfait);
    }
    public function passe(){
        $verbesPasse = Verbe::where('temps', '=', 'passe')
            ->get(['je', 'tu', 'il', 'nous', 'vous', 'ils'])
            ->toArray();
        dd($verbesPasse);
    }

    public function conditionnel(){
        $verbesConditionnel = Verbe::where('temps', '=', 'conditionnel')
            ->get(['je', 'tu', 'il', 'nous', 'vous', 'ils'])
            ->toArray();
        dd($verbesConditionnel);
    }

    public function futur(){
        $verbesFutur = Verbe::where('temps', '=', 'futur')
            ->get(['je', 'tu', 'il', 'nous', 'vous', 'ils'])
            ->toArray();
        dd($verbesFutur);
    }

}

<?php


namespace App\Http\Controllers;

use App\Answer;
use App\Post;
use App\Question;
use App\User;
use App\Verbe;
use Illuminate\Http\Request;
use App\Quiz;
use Illuminate\Support\Facades\DB;

class ConjugationTableController extends Controller
{
    //renvoie les données d'une table de conjugaison
    public function show(Request $request)
    {
        $userId = auth()->user()->id;
        $verbId = $request['verbId'];
        $verbInf = $request['verbInf'];

        $verbTable = DB::table('verbes')->where('infinitif_id', $verbId)->get()->toArray();

        return response()->json([
            'ok' => true,
            'tableDatas' => [$verbTable, $verbInf]
        ]);
    }

}

<?php


namespace App\Http\Controllers;

use App\Answer;
use App\Post;
use App\Question;
use App\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostQuizController extends Controller
{
    public function storeQuizLesson(Request $request)
    {
        $this->middleware('auth');

        $userId = auth()->user()->id;
        $postId = $request['postId'];

        //register quiz multiple
        $userAnswersQuizMultiple = $request['quizMultipleAnswers'];
        foreach($userAnswersQuizMultiple as $singleAnswer){
            //register the question
            $question = new Question();
            $question->user_id = $userId;
            $question->post_id = $postId;
            $question->label = $singleAnswer['question_label'];
            $question->save();

            //register answer
            $questionId = $question->id;
            $answer = new Answer();
            $answer->question_id = $questionId;
            $answer->userAnswerLabel = $singleAnswer['userChoice'];
            $answer->goodAnswerLabel = $singleAnswer['goodAnswer'];
            $answer->save();
        }

        //registering quiz open
        $userAnswersQuizOpen = $request['quizOpenAnswers'];
        foreach($userAnswersQuizOpen as $singleAnswer){
            //register the question
            $question = new Question();
            $question->user_id = $userId;
            $question->post_id = $postId;
            $question->label = $singleAnswer['question_label'];
            $question->save();

            //register answer
            $questionId = $question->id;
            $answer = new Answer();
            $answer->question_id = $questionId;
            $answer->text = $singleAnswer['userChoice'];
            $answer->save();
        }


        //update pivot is_answered field
        DB::table('users_posts')
            ->where('user_id', $userId)
            ->where('post_id', $postId)
            ->update(['is_answered' => true]);

        return response()->json([
            'ok' => true,
            'msg' => 'saved'
        ]);
    }


    /**
     * Vérifie si ce postQuiz a déjà été répondu par un user
     */
    public function isAlreadyAnswered(Request $request)
    {
        $userId = auth()->user()->id;
        $postId = $request['postId'];

        $useranswer = DB::table('questions')
            ->where('user_id', '=', $userId)
            ->where('post_id', '=', $postId)
            ->count();

        $isAlreadyAnswered = $useranswer > 0;

        $questions = null;
        $questionsFinal = ['questions_open' => [], 'questions_multiples' => []];


        if($isAlreadyAnswered)
        {
            //si le post a déjà été répondu, on récupère
            $questions = Question::where([
                ['user_id', '=', $userId],
                ['post_id', '=', $postId]
            ])->get();


            //récupère les réponse de chaqu euser
            foreach($questions as &$question)
            {
                $answer = Answer::where('question_id', $question->id)->first();
                $question->answer = $answer;
                if($answer->correction || $answer->text){ //si la rép contient une correction c'est donc une question open
                    $questionsFinal['questions_open'][] = $question;
                }else{
                    $questionsFinal['questions_multiples'][] = $question;
                }
            }
        }



        return response()->json([
            'ok' => true,
            'isAlreadyAnswered' => $isAlreadyAnswered,
            'userQuestionsAndAnswers' => $questionsFinal
        ]);
    }

    private function defineScoreForEachQuestions(&$userQuizzes)
    {
        $score = 0;
        foreach($userQuizzes as &$singleQuiz){
            $nbQuestions = $singleQuiz->nb_questions;

            foreach ($singleQuiz->questions as $index => $singleQuestion){
                if($singleQuestion->defineScore()){
                    ++$score;
                }
                $singleQuiz->score = $this->defineScorePercentage($score, $nbQuestions);
            }
            $score = 0;
        }
        return $userQuizzes;
    }
}

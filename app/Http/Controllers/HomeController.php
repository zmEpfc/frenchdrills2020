<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Post;
use App\Question;
use App\User;
use App\Verbe;
use Illuminate\Http\Request;
use App\Quiz;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->hasRole(['teacher'])){
            return $this->indexTeacher();
        }
        $userId = auth()->user()->id;

        $userQuizzes = Quiz::with([
            'questions' => function ($question) use($userId) {
            $question->where('user_id', '=', $userId);
        },
            'questions.answer'])->get();


        $userQuizzes = $userQuizzes->filter(function($quiz){
            return (count($quiz->questions) > 0 && $quiz->questions[0]->user_id === auth()->user()->id);
        });

        $userQuizzes = $userQuizzes->values(); //ré-ordonne les index

        $userQuizzes = $this->defineScoreForEachQuestions($userQuizzes);


        $statsWrongAnswerByPronoun = Quiz::statsWrongAnswerByPronoun($userId);
        $statsWrongAnswerByTenses = Quiz::statsWrongAnswerByTenses($userId);
        $statsWrongAnswerByInfinitive = Quiz::statsWrongAnswerByInfinitive($userId);

        $posts = $this->getUserPosts($userId);
        $posts = $this->assigneAuthorAndexcerptAndIsAnsweredAndIsCorrected($posts, $userId); //used in StudentLessonsVue.vue

        $role = auth()->user()->hasRole(['student']) ? 'student' : 'anonymous';

        //todo ici construire quizz faiblesse
        //1 trouver les ids des verbes quiz faiblesses
        //$selectedInfinitifsIds
        $quizFaiblesses = new Quiz();
        $infinitifsLabels = array_values(array_unique($statsWrongAnswerByInfinitive['infinitifsForQuizFaiblesse']));
        $quizFaiblesseSelectedInfinitifsIds =  DB::table('verbes_infinitif')
            ->whereIn('label', $infinitifsLabels)->pluck('id');
        unset($statsWrongAnswerByInfinitive['infinitifsForQuizFaiblesse']);

        if($statsWrongAnswerByTenses){
            $quizFaiblesseSelectedTenses = array_keys($statsWrongAnswerByTenses);
            $quizFaiblesses->buildQuizVerbMultipleChoices($quizFaiblesseSelectedInfinitifsIds, $quizFaiblesseSelectedTenses, 10, 4);
        } else{
            $quizFaiblesseSelectedTenses = null;
        }


        return view('home', [
            'userQuizzes' => $userQuizzes,
            'statsWrongAnswerByPronoun' => $statsWrongAnswerByPronoun,
            'statsWrongAnswerByTenses' => $statsWrongAnswerByTenses,
            'statsWrongAnswerByInfinitive' => $statsWrongAnswerByInfinitive,
            'posts' => $posts,
            'role' => $role,
            'infinitifs' => $this->quizVerbView(), //les infinitifs de départ
            'quizFaiblesses' => $quizFaiblesses,

            // pour teacher profile
            'treeViewDatas' => null,
        ]);
    }

    //get refreshed datas when a user clicks on Vuetify menu
    public function getRefreshedDatasStudent()
    {

        $userId = auth()->user()->id;

        $userQuizzes = Quiz::with([
            'questions' => function ($query) {
                $query->where('user_id', '=', auth()->user()->id);
            },
            'questions.answer'])->get();

        $userQuizzes = $userQuizzes->filter(function($quiz){
            return (count($quiz->questions) > 0 && $quiz->questions[0]->user_id === auth()->user()->id);
        });

        $userQuizzes = $userQuizzes->values(); //ré-ordonne les index

        $userQuizzes = $this->defineScoreForEachQuestions($userQuizzes);

        $statsWrongAnswerByPronoun = Quiz::statsWrongAnswerByPronoun($userId);
        $statsWrongAnswerByTenses = Quiz::statsWrongAnswerByTenses($userId);
        $statsWrongAnswerByInfinitive = Quiz::statsWrongAnswerByInfinitive($userId);

        return response()->json([
            'userQuizzes' => $userQuizzes,
            'statsWrongAnswerByPronoun' => $statsWrongAnswerByPronoun,
            'statsWrongAnswerByTenses' => $statsWrongAnswerByTenses,
            'statsWrongAnswerByInfinitive' => $statsWrongAnswerByInfinitive,
            'ok' => true
        ]);
    }

    //get all posts the user is allowed to do
    private function getUserPosts($userId)
    {
        return Post::all()->filter(function($v, $k) use($userId){
            $users = $v->users->toArray();
            $users = array_map(function($v){ return $v['id']; }, $users);
            return in_array($userId, $users);
        });
    }


    /*
    * Assigne des variables necessaires pour le composant StudentLessons.Vue
    * A savoir l'excerpt, l'authoeur, is_answered et is_corrected
    * et converti la date pour un affichage plus beau
    */
    private function assigneAuthorAndexcerptAndIsAnsweredAndIsCorrected($posts, $userId){
        foreach($posts as &$post){
            $authorName = User::where('id', $post->author)->pluck('name')[0];
            $post->authorName = $authorName; //assign expercpt
            $post->excerpt = $this->getExcerpt($post->content); //assign author
            // converti date
            $post->date = date_format(date_create($post->created_at),"d/m/Y");


            $isAnsweredAndCorrected = DB::table('users_posts')
                ->where('user_id', $userId)
                ->where('post_id', $post->id)
                ->select('is_answered', 'is_corrected')
                ->get()->first();

            $post->isAnswered = $isAnsweredAndCorrected->is_answered;
            $post->isCorrected = $isAnsweredAndCorrected->is_corrected;
        }

        return $posts;
    }


    private function getExcerpt($str, $startPos=0, $maxLength=150) {
        if(strlen($str) > $maxLength) {
            $str = strip_tags($str);
            $excerpt   = substr($str, $startPos, $maxLength-3);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= '...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    public function indexTeacher()
    {
        $userId = auth()->user()->id;

        //get posts of that teacher
        $posts = Post::where('author', $userId)->get();

        $treeviewDatas = []; //treeview on teacher homepage

        foreach($posts as $index => &$post)
        {
            $users = $post->users;
            //build treeViewDatas
            array_push($treeviewDatas, [
                'id' => $post->id,
                'name' => $post->name,
                'type' => 'post',
            ]);

            //ids de user étant autorisé à faire le quiz
            //et l'ayant vraiment fait. Car certains user sont invités mais ne l'ont pas
            //encore fait.
            $idsUserToKeep= [];

            foreach($users as &$user)
            {
                //buld $posts datas displayed on home-teacher
                $questions = Question::where([
                    ['user_id', '=', $user->id],
                    ['post_id', '=', $post->id]
                ])->get();

                //will attach questions of that user to the post displayed in home-teacher
                foreach($questions as &$question)
                {
                    $answer = Answer::where('question_id', $question->id)->first();
                    $question->answer = $answer;
                }
                $user->questions = $questions;

                //si ce user est autorisé à participer && a effectivement répondu au quiz
                if(count($questions) != 0)
                {
                    $idsUserToKeep[] = $user->id; //servira pour filtrer les users à afficher
                    //build treeViewDatas
                    $treeviewDatas[$index]['children'][] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'type' => 'student',
                        'post' => $post,
                        'student' => $user
                    ];
                }
            }
            //ne garde que les users respectant ces 2 conditions:
            // 1. user a le droit de faire le quiz
            // 2. user a effectivement passe le quiz donc iln'est pas en attente
            $users = $users->filter(function($v) use($idsUserToKeep) {
                return in_array($v->id, $idsUserToKeep);
            });
            unset($post->users);
            $post->users = $users;
        }

        $role = auth()->user()->hasRole(['teacher']) ? 'teacher' : 'anonymous';

        $treeviewDatas = array_filter($treeviewDatas, function($v){
            return array_key_exists('children', $v) && count($v['children']) > 0;
        });

        return view('home', [
            'posts' => $posts,
            'treeViewDatas' => $treeviewDatas,
            'role' => $role,

            //pour home profile
            'userQuizzes' => null,
            'statsWrongAnswerByPronoun' => null,
            'statsWrongAnswerByTenses' => null,
            'statsWrongAnswerByInfinitive' => null,
            'infinitifs' => $this->quizVerbView(),
            'quizFaiblesses' => null,
        ]);
    }

    private function defineScoreForEachQuestions(&$userQuizzes)
    {
        $res = [];
        $score = 0;
        foreach($userQuizzes as &$singleQuiz){
            $nbQuestions = $singleQuiz->nb_questions;

            foreach ($singleQuiz->questions as $index => &$singleQuestion){
                if($singleQuestion->defineScore()){
                    ++$score;
                }

            }
            $score =  $this->defineScorePercentage($score, $nbQuestions);
            $score = is_float($score) ? number_format($score, 2) : $score;
            $singleQuiz->score = $score;

            $res[] = $singleQuiz;
            $score = 0;
        }

        return $res;
    }

    private function defineScorePercentage($score, $nbQuestions){
        $nbQuestions = intval($nbQuestions);

        if($nbQuestions == 5){
            return $score * 20;
        }
        else if($nbQuestions == 10){
            return $score * 10;
        }
        else if($nbQuestions == 15){
            return ($score * 10) / 1.5;
        }
        else if($nbQuestions == 20){
            return $score * 5;
        }
        else if($nbQuestions == 25){
            return $score * 4;
        }
        else if($nbQuestions == 30){
            return ($score * 10) / 3;
        }
        else if($nbQuestions == 35){
            return ($score * 20) / 7;
        }
        else if($nbQuestions == 35){
            return ($score / 3.5) * 10;
        }
        else if($nbQuestions == 40){
            return $score * 2.5;
        }
        else if($nbQuestions == 45){
            return $score / 4.5 * 10;
        }
        else if($nbQuestions == 50){
            return $score * 2;
        }
    }

    //pour afficher infinitifs quizz libre <quiz-verb-multiple-choices> sur student-profil
    public function quizVerbView()
    {
        $infinitifsDatas = DB::table('verbes_infinitif')->orderBy('label')->select('id', 'label')
            ->orderBy('label')->get();
        $infinitifsDatas = json_decode($infinitifsDatas, true);
        $infinitifsIds = array_column($infinitifsDatas, 'id');
        $infinitifsLabel = array_column($infinitifsDatas, 'label');
        $infinitifsLabel = str_replace(':', '', $infinitifsLabel);
        $infinitifsLabel = array_map('ucfirst', $infinitifsLabel);
        $final = array_combine($infinitifsIds, $infinitifsLabel);
        return json_encode($final);
//        return view('quiz.verb.view', [
//            'infinitifs' => json_encode($final),
//        ]);
    }
}

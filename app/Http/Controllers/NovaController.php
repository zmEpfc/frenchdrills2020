<?php


namespace App\Http\Controllers;
use App\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NovaController extends Controller
{
    public function getWorstVerbs(){
        $statsWrongAnswerByInfinitive = Quiz::statsWrongAnswerByInfinitive();

        $statsWrongAnswerByInfinitive = array_filter($statsWrongAnswerByInfinitive, function($v){
            return !is_array($v);
        });

        $statsWrongAnswerByInfinitive = array_slice($statsWrongAnswerByInfinitive, 0, 5);

        return response()->json([
            'statsWrongAnswerByInfinitive' => $statsWrongAnswerByInfinitive,
            'ok' => true
        ]);
    }
}

<?php


namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeTeacherController extends Controller
{
    public function storeCorrection(Request $request)
    {
        $answerId = $request['answerId'];
        $correction = $request['correction'];
        $userId = $request['userId'];
        $postId = $request['postId'];

        //update
        $answer = Answer::find($answerId);
        $answer->correction = $correction;
        $answer->save();

        //update _corrected field
        DB::table('users_posts')
            ->where('user_id', $userId)
            ->where('post_id', $postId)
            ->update(['is_corrected' => true]);


        return response()->json([
            'ok' => true,
            'msg' => 'updated'
        ]);
    }
}

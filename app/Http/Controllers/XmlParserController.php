<?php

namespace App\Http\Controllers;

use App\Verbe;
use App\VerbeConditionnelPresent;
use App\VerbeFutur;
use App\VerbeImparfait;
use App\VerbePasseSimple;
use App\VerbePresent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\VerbeInfinitif;


class XmlParserController extends Controller
{
    public function dd($data) { var_dump($data); }

    public function index()
    {
        $file = (storage_path() . "/app/public/conjugaison.xml");
        $xmldata = simplexml_load_file($file) or die("Failed to load");
        $xmldata = json_decode( json_encode($xmldata) , 1);
        $index=0;

        foreach($xmldata["template"] as $singleVerb)
        {
            ++$index;

            //récupère infinitif
            $infinitfLabel = $singleVerb["@attributes"]["name"];
            // récupère l'id de l'infitif en cours
            $verbeInfinitifId = VerbeInfinitif::where('label', $infinitfLabel)->first()->id;

            if($verbeInfinitifId) //si on a pu récupère l'id de la bdd
            {
              $arrConditionnel = $singleVerb["conditionnel"]["present"]["p"];

                $verbe = new Verbe();
                $verbe->infinitif_id = $verbeInfinitifId;
                $verbe->temps = 'conditionnel';

                $radical = explode(':', $infinitfLabel);

                //on boucle à l'interieur de chaque verbe
                $k = 0;

                foreach($arrConditionnel as $singlePronom)
                {
                    if(!array_key_exists("i", $singlePronom)){
                        $this->dd($infinitfLabel); die('le script a été tué');
                    }
                    $pronom = $this->getPronom($k);
                    $apresRadical = $singlePronom["i"];
                    ++$k;

                    $ligneFinale = "$radical[0]$apresRadical";

                    $verbe->$pronom = $ligneFinale;
                }

                $verbe->save();

            } else echo "problème de récupération id";
        }
    }

    private function checkErrorLigneFinale($ligneFinale)
    {
        if(!$ligneFinale){
            echo "ligne finale est null";
        } elseif(strlen($ligneFinale) < 1){
            echo "ligne finale est de taille inferieure a 1";
            echo $ligneFinale;
        } elseif($ligneFinale === ""){
            echo "ligne finale est vide";
        }
    }

    private function getPronom($index){
        switch($index){
            case 0 : return 'je';
            case 1 : return 'tu';
            case 2 : return 'il';
            case 3 : return 'nous';
            case 4 : return 'vous';
            case 5 : return 'ils';
        }
    }

    // note: au moment d'insérer les infinitifs je n'avais pas vu que je
    // pouvais encoder les données en json.
    private function insertInfinitifs($infinitfLabel)
    {
        $verbeInfinitif = new VerbeInfinitif();
        $verbeInfinitif->label = $infinitfLabel;
        $verbeInfinitif->save();
    }
}

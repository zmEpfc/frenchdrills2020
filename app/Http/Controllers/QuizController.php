<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\Question;
use App\Answer;
use App\VerbeInfinitif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class QuizController extends Controller
{


    public function index()
    {
        //
    }

    public function create()
    {
        return view('quizzes.create');
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $quiz = Quiz::find($id);

        if(!auth()->user() || !$this->defineCanSee($quiz)){
            return view('quizzes.forbidden');
        }

        $userId = auth()->user()->id;
//        $isAlreadyAnswered = Question::where('user_id', $userId)
//                ->where('quiz_id', $id)->count() > 0;
        $isAlreadyAnswered = false;

        if($isAlreadyAnswered){
//            $quizAlreadyAnsweredDatas = Quiz::with(['questions' => function ($query) {
//                $query->where('user_id', '=', auth()->user()->id);
//            }])->get();

            //todo créer une page 'vous avez déjà vu ce message' et voici votre score
            //il faut créer cela sur un page blade dédiée et on utilise le composant Score
            $quiz = Quiz::find($id);
            return view('quizzes.verb.already_answered');
        }


        $type = $quiz->type;
        $selectedInfinitifsIds = $this->parseArray($quiz->verbes);
        $selectedTenses = $this->parseArray($quiz->tenses);
        $selectedTenses = $this->convertTensesSelectList($selectedTenses);
        $nbQuestions = $quiz->nb_questions;
        $nbChoices = $quiz->nb_choices;

        $currentMaxQuestion = count($selectedTenses) * count($selectedInfinitifsIds) * 5;

        if($currentMaxQuestion < $nbQuestions){
            $nbQuestions = $currentMaxQuestion;
            $quiz->nb_questions = $nbQuestions;
        }


        if($type === 'multiple')
        {
            $quiz->buildQuizVerbMultipleChoices($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices);
            $template = 'quizzes.verb.multiples_choices';
        } elseif($type === 'missing'){
            $quiz->buildQuizMissing($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices);
            $template = 'quizzes.verb.quiz_missing';
        }



        return view($template, [
            'quiz' => $quiz,
            'nbChoices' => $nbChoices,
            'nbQuestions' => $nbQuestions,
            'ok' => true
        ]);

    }

    //convert from nova <select>
    private function convertTensesSelectList($arrTenses)
    {
        return array_map(function($val){
            $mask = ["present", "imparfait", "futur", "passe", "conditionnel"];
            return $mask[$val];
        }, $arrTenses);
    }

    //format "["3", "4"]" deviendra [3, 4]
    private function parseArray($var)
    {
        $var = explode(',', $var);
        return array_map(function($val){ return filter_var($val, FILTER_SANITIZE_NUMBER_INT); }, $var);
    }

    private function defineCanSee($quiz)
    {
        $authorizedIds = $quiz->users->pluck('id')->toArray();
        $currentUserId = auth()->user()->id;
        return in_array($currentUserId, $authorizedIds);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->middleware('auth');

        $quiz = Quiz::find($id);
        $quizForScore = $request->quizForScore;

        for($i=0; $i<count($quizForScore); ++$i)
        {
            //register question
            $questionData = $quizForScore[$i];
            $quest = new Question();
            $quest->label = $questionData["label"];
            $quest->position = $questionData["position"];
            $quest->quiz_id = $id;
            $quest->user_id = auth()->user()->id;
            $quest->save();
            $questionId = $quest->id;

            //register userAnswer
            $answer = new Answer();
            $answer->userAnswerLabel = $questionData["userAnswer"]["label"];
            $answer->goodAnswerLabel = $questionData["userAnswer"]["goodAnswerLabel"];
            $answer->question_id = $questionId;
            $answer->save();
        }

        return response()->json([
            'ok' => true,
            'msg' => 'saved'
        ]);

    }

    //quand on save le quiz
    public function storeQuizVerb(Request $request)
    {
        $this->middleware('auth');
        $quizForScore = $request->quizForScore;
        $selectedTenses = $request->selectedTenses;
        $selectedInfinitifs = $request->selectedInfinitifs;
        //save Quizz
        $quiz = new Quiz();
        $quiz->type = 'Quiz Libre';
        $quiz->verbes = join(',', $selectedTenses);
        $quiz->tenses = join(',', $selectedInfinitifs);
        $quiz->nb_questions = count($quizForScore);
        $quiz->save();
        $quizId = $quiz->id;

        for($i=0; $i<count($quizForScore); ++$i)
        {
            //register question
            $questionData = $quizForScore[$i];
            $quest = new Question();
            $quest->label = $questionData["label"];
            $quest->position = $questionData["position"];
            $quest->quiz_id = $quizId;
            $quest->user_id = auth()->user()->id;
            $quest->save();
            $questionId = $quest->id;

            //register userAnswer
            $answer = new Answer();
            $answer->userAnswerLabel = $questionData["userAnswer"]["label"];
            $answer->goodAnswerLabel = $questionData["userAnswer"]["goodAnswerLabel"];
            $answer->question_id = $questionId;
            $answer->save();
        }

        return response()->json([
            'ok' => true,
            'msg' => 'saved'
        ]);
    }

    public function destroy($id)
    {
        //
    }

    public function vv($data = null)
    {
        var_dump($data);
    }

    private function stripslashes_array(&$arr) {
        array_walk_recursive($arr, function (&$val) {
            $val = htmlentities($val, ENT_QUOTES);
        });
    }



    //on affiche le quiz
    public function quizVerbView(Request $request)
    {
        $infinitifsDatas = DB::table('verbes_infinitif')->orderBy('label')->select('id', 'label')
            ->orderBy('label')->get();
        $infinitifsDatas = json_decode($infinitifsDatas, true);
        $infinitifsIds = array_column($infinitifsDatas, 'id');
        $infinitifsLabel = array_column($infinitifsDatas, 'label');
        $infinitifsLabel = str_replace(':', '', $infinitifsLabel);
        $infinitifsLabel = array_map('ucfirst', $infinitifsLabel);
        $final = array_combine($infinitifsIds, $infinitifsLabel);
        return view('quiz.verb.view', [
            'infinitifs' => json_encode($final),
        ]);
    }

    public function freeQuizCreate(Request $request)
    {
        $selectedTenses =  $request->tenses;
        $selectedInfinitifsIds = $request->infinitifsIds;
        $nbQuestions = $request->nbQuestions;
        $nbChoices = $request->nbChoices;

        $quiz = new Quiz();
//        dd($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices);

        $quiz->buildQuizVerbMultipleChoices($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices);

        return response()->json([
            'quiz' => $quiz,
            'ok' => true
        ]);
    }


}

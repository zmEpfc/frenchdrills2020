<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function quizzes_custom()
    {
        return $this->belongsToMany('App\QuizCustom', 'quizzes_custom');
    }

    public function quizzes()
    {
        return $this->belongsToMany('App\Quiz', 'users_quizzes');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Posts', 'users_posts');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function (User $user) {
            //if created by command line then should be null
            $isTeacher = !empty(auth()->user()->id);
            $role = $isTeacher ? 'student' : 'teacher';
            $user->assignRole($role);
        });
    }
}

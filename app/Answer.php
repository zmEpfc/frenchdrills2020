<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 *  Class Choice
 * This is the model class for table "policy"
 *
 * @property integer $id
 * @property string $userAnswerLabel
 * @property string $goodAnswerLabel
 * @property integer $question_id
 */
class Answer extends Model
{
    public $timestamps = false;

    public function question(){
        return $this->belongsTo(Question::class);
    }

}

<?php


namespace App\Policies;

namespace App\Policies;

use App\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    public function viewAny(User $user)
    {
        return $user->hasAnyRole(['admin', 'teacher']);
    }
}

<?php

namespace App\Policies;

use App\Quiz;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuizPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $user->hasAnyRole(['none',]);

    }

    public function view(User $user)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user)
    {
        return true;
//        return $user->id === $post->user_id;
    }

    public function delete(User $user)
    {
        return true;
//        return $user->id === $post->user_id;
    }


    public function restore(User $user)
    {
        return true;
    }


    public function forceDelete(User $user)
    {
        return true;
    }

    public function attachTag(User $user, Quiz $quiz, User $handler)
    {
        return true;
    }

    /**
     * Determine whether the user can attach any users to the Quiz.
     *
     * @param  \App\User  $user
     * @param  \App\Quiz  $quiz
     * @return mixed
     */
    public function attachAnyUser(User $user, Quiz $quiz)
    {
        return true;
    }

}

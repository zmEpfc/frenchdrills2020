<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verbe extends Model
{
    protected $table = 'verbes';
    public $timestamps = false;
}

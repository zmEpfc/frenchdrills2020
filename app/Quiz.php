<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

/**
 *  Class Question
 * This is the model class for table "policy"
 *
 * @property integer $id
 * @property string $type
 * @property Collection $questions
 * @property integer $user_id
 */
class Quiz extends Model
{
    public $timestamps = false;

    protected $hidden = [];

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_quizzes');
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function buildQuizMissing($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices)
    {
        $verbes = $this->makeQuery($selectedInfinitifsIds, $selectedTenses);
        return $this->buildQuestionsQuizMissing($verbes, $nbQuestions, $nbChoices);

    }

    /**
     * @param $selectedTenses : les temps de conjugaison choisis par l'utilisateur
     * @param $selectedInfinitifsIds : les verbes choisis par le user via <select> list
     * @return array
     */
    public function buildQuizVerbMultipleChoices($selectedInfinitifsIds, $selectedTenses, $nbQuestions, $nbChoices)
    {
        $verbes = $this->makeQuery($selectedInfinitifsIds, $selectedTenses);
        return $this->buildQuestions($verbes, $nbQuestions, $nbChoices);
    }


    private function makeQuery($selectedInfinitifsIds, $selectedTenses)
    {
        $verbes = DB::table('verbes')
            ->whereIn('infinitif_id', $selectedInfinitifsIds)
            ->whereIn('temps', $selectedTenses)
            ->join('verbes_infinitif', 'verbes_infinitif.id', '=', 'verbes.infinitif_id')
            ->get();

        return $verbes->toArray();
    }

    private function buildQuestionsQuizMissing($verbes, $nbQuestions, $nbChoices)
    {
        $quizFinalQuestions = [];

        foreach($verbes as $singleVerb)
        {
            $singleVerb = (array) $singleVerb;

            $filteredVerbs = Question::filterPronouns($singleVerb);

            $question = new Question();
            $questions = $question->buildQuestionsFromSingleVerbQuizMissing($singleVerb);
            $quizFinalQuestions[] = $questions;
        }

        //merge all questions in 1 single array
        $quizFinalQuestions = array_reduce($quizFinalQuestions, 'array_merge', array());
        shuffle($quizFinalQuestions);
        $quizFinalQuestions = array_slice($quizFinalQuestions, 0, $nbQuestions);
        $quizFinalQuestions = array_chunk($quizFinalQuestions, 5);
        $this->questions = $quizFinalQuestions;
    }

    private function buildQuestions($verbes, $nbQuestions, $nbChoices)
    {
        $quizFinalQuestions = [];

        foreach($verbes as $singleVerb)
        {
            $singleVerb = (array) $singleVerb;

            $filteredVerbs = Question::filterPronouns($singleVerb);

            $question = new Question();
            $questions = $question->buildQuestionsFromSingleVerb($singleVerb, $nbChoices);
            $quizFinalQuestions[] = $questions;
        }

        //merge all questions in 1 single array
        $quizFinalQuestions = array_reduce($quizFinalQuestions, 'array_merge', array());
        shuffle($quizFinalQuestions);

        $quizFinalQuestions = array_slice($quizFinalQuestions, 0, $nbQuestions);
        $quizFinalQuestions = array_chunk($quizFinalQuestions, 5);

        $this->questions = $quizFinalQuestions;

    }

    public static function statsWrongAnswerByPronoun($userId){
        $res = ['je'=>0, 'tu'=>0, 'il'=>0, 'nous'=>0, 'vous'=>0, 'ils'=>0];
        $quizzes = self::getAllQuizzesByCurrentUserId();

        foreach($quizzes as $quiz){

            //pour chaque question de ce quiz
            foreach($quiz->questions as $question){
                $pronoun = explode(',', $question->label)[0];
                $answer = $question->answer;
                $isCorrect = $answer->userAnswerLabel === $answer->goodAnswerLabel;
                if(!$isCorrect){
                    $res[$pronoun] = $res[$pronoun] + 1;
                }
            }
        }

        //vérifie s'il y a des statistiques différentes.
        $hasStats = array_diff_assoc($res, ['je'=>0, 'tu'=>0, 'il'=>0, 'nous'=>0, 'vous'=>0, 'ils'=>0]);
        if(count($hasStats) == 0){
            $res = false;
        } else{
            arsort($res);
        }
        return $res;
    }

    public static function statsWrongAnswerByTenses($userId){
        $res = ['present'=>0, 'imparfait'=>0, 'futur'=>0, 'passe'=>0, 'conditionnel'=>0];
        $quizzes = self::getAllQuizzesByCurrentUserId();

        foreach($quizzes as $quiz){

            //pour chaque question de ce quiz
            foreach($quiz->questions as $question){
                $tense = explode(',', $question->label)[2];
                $answer = $question->answer;
                $isCorrect = $answer->userAnswerLabel === $answer->goodAnswerLabel;
                if(!$isCorrect){
                    $res[$tense] = $res[$tense] + 1;
                }

            }
        }

        //vérifie s'il y a des statistiques différentes.
        $hasStats = array_diff_assoc($res, ['present'=>0, 'imparfait'=>0, 'futur'=>0, 'passe'=>0, 'conditionnel'=>0]);
        if(count($hasStats) == 0){
            $res = false;
        } else{
            arsort($res);
        }
        return $res;
    }

    public static function statsWrongAnswerByInfinitive($userId = null){
        $res = [];
        $infinitifQuizFaiblesse = []; //sera utilisé dans Homecontroller@indexAction() vers la fin;

        if($userId){
            $quizzes = self::getAllQuizzesByCurrentUserId();
        } else{
            $quizzes = self::getAllQuizzes();
        }

        foreach($quizzes as $quiz){

            //pour chaque question de ce quiz
            foreach($quiz->questions as $question){
                $infinitif = explode(',', $question->label)[1];
                $infinitifQuizFaiblesse[] = $infinitif;
                $infinitif = str_replace(':', '', $infinitif);

                $answer = $question->answer;
                $isCorrect = $answer->userAnswerLabel === $answer->goodAnswerLabel;
                if(!$isCorrect){
                    if(!array_key_exists($infinitif, $res)){
                        $res[$infinitif] = 1;
                    } else{
                        $res[$infinitif] = $res[$infinitif]+1;
                    }
                }
            }
        }

        if(count($res) == 0){
            $res = false;
        } else{
            arsort($res);
        }

        $res['infinitifsForQuizFaiblesse'] = $infinitifQuizFaiblesse;
        return $res;
    }

    private static function getAllQuizzesByCurrentUserId(){
        return Quiz::with([
            'questions' => function ($query) {
                $query->where('user_id', '=', auth()->user()->id);
            },
            'questions.answer'])->get();
    }

    private static function getAllQuizzes(){
        return Quiz::with([
            'questions' => function ($query) {
            },
            'questions.answer'])->get();
    }



}

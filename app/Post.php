<?php

namespace App;

use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Post extends Model
{
    protected $guarded = ['id', 'created_at', 'slug'];

    public function __construct()
    {
        $this->author = auth()->user()->id;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
//            $post->user_id = auth()->user()->id;
            $post->created_at = Carbon::now('Europe/Brussels');
        });

    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_posts');
    }

//    public function categories(){
//        return $this->belongsToMany(Category::class);
//    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

}

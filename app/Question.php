<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Collection;

/**
 *  Class Question
 * This is the model class for table "policy"
 *
 * @property integer $id
 * @property integer $position
 * @property string $label
 * @property string $labelAudio
 * @property Collection $choices
 * @property integer $quizId
 * @property integer $post_id
 * @property integer $user_id
 */
class Question extends Model
{
    protected $table = 'questions';
    public $timestamps = false;

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function answer()
    {
        return $this->hasOne('App\Answer');
    }

    public function buildQuestionsFromSingleVerb($singleVerb, $nbChoices)
    {
        $infinitif = $singleVerb['label'];
        $pronounsAndValues = self::filterPronouns($singleVerb);
        $temps = $singleVerb['temps'];
        $questions = [];

        foreach ($pronounsAndValues as $pronounKey => $pronounVal){
            $quest = new Question();
            $quest->label = "$pronounKey,$infinitif,$temps";
            $quest->choices = Choice::buildChoices($pronounKey, $pronounsAndValues, $nbChoices, $quest->position);
            array_push($questions, $quest);
        }
        shuffle($questions);
        self::defineQuestionsPosition($questions);
        return $questions;
    }

    public function buildQuestionsFromSingleVerbQuizMissing($singleVerb)
    {
        $infinitif = $singleVerb['label'];
        $pronounsAndValues = self::filterPronouns($singleVerb);
        $temps = $singleVerb['temps'];
        $questions = [];

        foreach ($pronounsAndValues as $pronounKey => $pronounVal){
            $quest = new Question();
            $quest->label = "$pronounKey,$infinitif,$temps";
            $quest->choices = Choice::buildChoices($pronounKey, $pronounsAndValues, 1, $quest->position, true);
            array_push($questions, $quest);
        }
        shuffle($questions);
        self::defineQuestionsPosition($questions);

        return $questions;
    }

    //define questions position after shuffle
    private static function defineQuestionsPosition(&$questions)
    {
        for($i=0; $i<count($questions); ++$i){
            $questions[$i]->position = $i;
        }
        return $questions;
    }

    public static function filterPronouns($singleVerb)
    {
        return array_filter($singleVerb, function($key){
            return $key != 'label' && $key != 'temps' && $key != 'infinitif_id' && $key != 'id';
        }, ARRAY_FILTER_USE_KEY);
    }

    private static function vv($data)
    {
        var_dump($data);
    }

    public function defineScore(){
        $this->hasScored = $this->answer->userAnswerLabel === $this->answer->goodAnswerLabel;
        return $this->answer->userAnswerLabel === $this->answer->goodAnswerLabel;
    }
}

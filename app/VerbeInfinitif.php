<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerbeInfinitif extends Model
{
    protected $table = 'verbes_infinitif';
    public $timestamps = false;
    protected $guarded = ['id'];

    public function verbes()
    {
        return $this->hasMany('App\Verbe');
    }


}

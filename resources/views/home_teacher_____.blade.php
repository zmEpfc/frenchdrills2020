@extends('layouts.app')

@section('content')
    <div class="container">
        
        <v-app>
            <profile-teacher :posts="{{$posts}}" :tree-view-datas="{{json_encode($treeViewDatas)}}"></profile-teacher>
        </v-app>
    </div>
@endsection

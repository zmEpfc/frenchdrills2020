@extends('layouts.app')

@section('content')
    <div class="container"
         data-infinitifs="{{$infinitifs}}"
         id="start"
         data-is-auth="{{Auth::id()}}"
    >
    <v-app>
{{--        <profile--student :user-quizzes="{{$userQuizzes}}"--}}
{{--                          :posts="{{json_encode($posts)}}"--}}
{{--                          :stats-wrong-per-verb="{{json_encode($statsWrongAnswerByInfinitive)}}"--}}
{{--                          :stats-wrong-per-tense="{{json_encode($statsWrongAnswerByTenses)}}"--}}
{{--                          :stats-wrong-per-pronoun="{{json_encode($statsWrongAnswerByPronoun)}}">--}}
{{--        </profile--student>--}}


        <home :role="{{json_encode($role)}}" :user-quizzes="{{json_encode($userQuizzes)}}"
              :posts="{{json_encode($posts)}}"
              :stats-wrong-per-verb="{{json_encode($statsWrongAnswerByInfinitive)}}"
              :stats-wrong-per-tense="{{json_encode($statsWrongAnswerByTenses)}}"
              :stats-wrong-per-pronoun="{{json_encode($statsWrongAnswerByPronoun)}}"
              :tree-view-datas="{{json_encode($treeViewDatas)}}"
              :quiz-faiblesses="{{json_encode($quizFaiblesses)}}"
        ></home>

    </v-app>
</div>

@endsection


@extends('layouts.app')

@section('content')
    <template>
        <div class="container"
             data-infinitifs="{{$infinitifs}}"
             id="start"
             data-is-auth="{{Auth::id()}}"
        >
            <quiz-verb-multiple-choices></quiz-verb-multiple-choices>
        </div>
    </template>
@endsection


@extends('layouts.app')

@section('content')
    @php
        $nbChoices = $nbChoices ?? 0;
    @endphp
    <template>
        <div class="container">
            <quiz-missing-verb :quiz="{{$quiz}}"
                                        :nb-questions="{{$nbQuestions}}" :nb-choices="{{$nbChoices}}"
                                        :steps="{{$nbQuestions/5}}">
            </quiz-missing-verb>
        </div>
    </template>
@endsection



@extends('layouts.app')

@section('content')
    <template>
        <div class="container">
            <quiz-multiple-choices-verb :quiz="{{$quiz}}"
                :nb-questions="{{$nbQuestions}}" :nb-choices="{{$nbChoices}}"
                :steps="{{$nbQuestions/5}}">
            </quiz-multiple-choices-verb>
        </div>
    </template>
@endsection

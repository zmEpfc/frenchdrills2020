/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');

import Vuetify from "../plugins/vuetify";
import VueCharts from "vue-chartjs";


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('quiz-verb-multiple-choices', require('./components/quizzes/QuizVerbMultipleChoices.vue').default);
Vue.component('quiz-verb-stepper', require('./components/quizzes/QuizVerbStepper.vue').default);
Vue.component('quiz-verb-score', require('./components/quizzes/QuizVerbScore.vue').default);
Vue.component('quiz-faiblesses', require('./components/quizzes/QuizFaiblesses.vue').default);

//home
Vue.component('home', require('./components/Home.vue').default);


//teacher components
Vue.component('quiz-multiple-choices-verb', require('./components/teacher/QuizMultipleChoicesVerb.vue').default);
Vue.component('quiz-missing-verb', require('./components/teacher/QuizMissingVerb.vue').default);

//profile components
Vue.component('profile-Student', require('./components/profile/ProfileStudent.vue').default);
Vue.component('history-datagrid', require('./components/profile/HistoryDatagrid.vue').default);
Vue.component('stats-wrong-by-infinitive', require('./components/profile/StatsWrongByInfinitive.vue').default);
Vue.component('stats-wrong-by-tense', require('./components/profile/StatsWrongByTense.vue').default);
Vue.component('stats-wrong-by-pronoun', require('./components/profile/StatsWrongByPronoun.vue').default);
Vue.component('student-lessons', require('./components/profile/StudentLessons.vue').default);
Vue.component('TableConjugation', require('./components/profile/TableConjugation.vue').default);


//profile teacher
Vue.component('profile-teacher', require('./components/profile/ProfileTeacher.vue').default);


//posts components
Vue.component('Post', require('./components/posts/Post.vue').default);
Vue.component('PostContent', require('./components/posts/PostContent.vue').default);
Vue.component('PostQuizz', require('./components/posts/PostQuizz.vue').default);
Vue.component('PostQuizLesson', require('./components/posts/PostQuizLesson.vue').default);
Vue.component('PostQuizQuestionsMultiples', require('./components/posts/PostQuizQuestionsMultiples.vue').default);
Vue.component('PostQuizQuestionsOpen', require('./components/posts/PostQuizQuestionsOpen.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    vuetify: Vuetify,
    el: '#app',
});
